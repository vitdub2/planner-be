import { SetMetadata } from '@nestjs/common';

export const IS_GRAPH_PUBLIC_KEY = 'IS_GRAPH_PUBLIC_KEY';
export const GraphPublic = () => SetMetadata(IS_GRAPH_PUBLIC_KEY, true);
