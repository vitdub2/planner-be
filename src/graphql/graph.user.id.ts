import { USER } from './graph.jwt.auth.guard';

import { createParamDecorator, ExecutionContext } from '@nestjs/common';

export const GraphUserId = createParamDecorator(
  (data: unknown, ctx: ExecutionContext) => {
    const user = Reflect.getMetadata(USER, ctx.getHandler());
    return user._id;
  },
);
