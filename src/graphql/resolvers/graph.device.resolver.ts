import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { GraphResolver } from './graph.public.resolver.decorator';
import { GraphDevice } from '../interfaces/graph.device';
import { DeviceService } from '../../device/device.service';
import { DeviceDto } from '../../device/device.interfaces';
import { GraphUserId } from '../graph.user.id';
import { UseGuards } from '@nestjs/common';
import { GraphJwtAuthGuard } from '../graph.jwt.auth.guard';

@Resolver((of) => GraphDevice)
@UseGuards(GraphJwtAuthGuard)
@GraphResolver()
export class GraphDeviceResolver {
  constructor(private readonly deviceService: DeviceService) {}

  @Mutation((returns) => GraphDevice)
  async createDevice(
    @Args('deviceDto') deviceDto: DeviceDto,
    @GraphUserId() userId: string,
  ) {
    return this.deviceService.createEntity(deviceDto, userId);
  }

  @Mutation((returns) => GraphDevice)
  async updateDevice(
    @Args('deviceDto') deviceDto: DeviceDto,
    @Args('deviceId') deviceId: string,
    @GraphUserId() userId: string,
  ) {
    return this.deviceService.changeEntity(deviceDto, userId, deviceId);
  }

  @Mutation((returns) => GraphDevice)
  async deleteDevice(@Args('deviceId') deviceId: string) {
    return this.deviceService.deleteEntity(deviceId);
  }
}
