import { IS_PUBLIC_KEY } from '../../utils/public.decorator';

export function GraphResolver(): ClassDecorator {
  return (target) => {
    Reflect.defineMetadata(IS_PUBLIC_KEY, true, target);
  };
}
