import { Args, Mutation, Resolver } from '@nestjs/graphql';
import { GraphResolver } from './graph.public.resolver.decorator';
import { GraphUserId } from '../graph.user.id';
import { GraphActivity } from '../interfaces/graph.activity';
import { ActivityService } from '../../activity/activity.service';
import { ActivityDto } from '../../activity/activity.interfaces';
import { UseGuards } from '@nestjs/common';
import { GraphJwtAuthGuard } from '../graph.jwt.auth.guard';

@Resolver((of) => GraphActivity)
@UseGuards(GraphJwtAuthGuard)
@GraphResolver()
export class GraphActivityResolver {
  constructor(private readonly activityService: ActivityService) {}

  @Mutation((returns) => GraphActivity)
  async createActivity(
    @Args('activityDto') activityDto: ActivityDto,
    @GraphUserId() userId: string,
  ) {
    return this.activityService.createEntity(activityDto, userId);
  }

  @Mutation((returns) => GraphActivity)
  async updateActivity(
    @Args('activityDto') activityDto: ActivityDto,
    @Args('activityId') activityId: string,
    @GraphUserId() userId: string,
  ) {
    return this.activityService.changeEntity(activityDto, userId, activityId);
  }

  @Mutation((returns) => GraphActivity)
  async deleteActivity(@Args('activityId') activityId: string) {
    return this.activityService.deleteEntity(activityId);
  }
}
