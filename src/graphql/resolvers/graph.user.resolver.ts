import {
  Args,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';
import { GraphUser } from '../interfaces/graph.user';
import { UserService } from '../../user/user.service';
import { ActivityService } from '../../activity/activity.service';
import { DeviceService } from '../../device/device.service';
import { GraphResolver } from './graph.public.resolver.decorator';
import { UseGuards } from '@nestjs/common';
import { GraphJwtAuthGuard } from '../graph.jwt.auth.guard';
import { GraphUserId } from '../graph.user.id';
import { GraphPublic } from '../graph.public.decorator';
import { UserCreateDto, UserDto } from '../../user/user.interfaces';

@Resolver((of) => GraphUser)
@GraphResolver()
@UseGuards(GraphJwtAuthGuard)
export class GraphUserResolver {
  constructor(
    private readonly userService: UserService,
    private readonly activityService: ActivityService,
    private readonly deviceService: DeviceService,
  ) {}

  @Query((returns) => GraphUser)
  async user(@GraphUserId() userId: string) {
    return this.userService.findUserById(userId);
  }

  @ResolveField()
  async devices(@Parent() user: GraphUser) {
    return this.deviceService.getAllEntitiesByUserId(user._id);
  }

  @ResolveField()
  async activities(@Parent() user: GraphUser) {
    return this.activityService.getAllEntitiesByUserId(user._id);
  }

  @Mutation((returns) => GraphUser)
  @GraphPublic()
  async createUser(@Args('userCreateDto') createDto: UserCreateDto) {
    return this.userService.createUser(createDto);
  }

  @Mutation((returns) => GraphUser)
  async updateUser(
    @Args('userDto') createDto: UserDto,
    @GraphUserId() userId: string,
  ) {
    return this.userService.updateUser(userId, createDto);
  }

  @Mutation((returns) => GraphUser)
  async deleteUser(@GraphUserId() userId: string) {
    return this.userService.deleteUser(userId);
  }
}
