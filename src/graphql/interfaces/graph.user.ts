import { Field, ObjectType } from '@nestjs/graphql';
import { GraphDevice } from './graph.device';
import { GraphActivity } from './graph.activity';

@ObjectType()
export class GraphUser {
  @Field()
  _id: string;

  @Field()
  username: string;

  @Field()
  email: string;

  @Field((type) => [GraphActivity])
  activities: GraphActivity[];

  @Field((type) => [GraphDevice])
  devices: GraphDevice[];
}
