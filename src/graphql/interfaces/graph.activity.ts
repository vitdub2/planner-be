import { Field, ObjectType } from '@nestjs/graphql';
import { Time } from '../../schema/activity.schema';
import { GraphBaseModel } from './graph.base.model';

@ObjectType()
export class GraphActivity extends GraphBaseModel {
  @Field()
  name: string;

  @Field({ nullable: true })
  desc: string;

  @Field((type) => [Time])
  times: Time[];
}
