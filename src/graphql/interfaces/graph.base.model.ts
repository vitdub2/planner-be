import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class GraphBaseModel {
  @Field()
  _id: string;
}
