import { Field, ObjectType } from '@nestjs/graphql';
import { GraphBaseModel } from './graph.base.model';

@ObjectType()
export class GraphDevice extends GraphBaseModel {
  @Field()
  token: string;

  @Field()
  name: string;

  @Field((type) => Boolean)
  shouldBeNotified: boolean;
}
