import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { JwtService } from '@nestjs/jwt';
import { Reflector } from '@nestjs/core';
import { IS_GRAPH_PUBLIC_KEY } from './graph.public.decorator';

export const USER = 'USER';

@Injectable()
export class GraphJwtAuthGuard implements CanActivate {
  constructor(
    private readonly jwtService: JwtService,
    private reflector: Reflector,
  ) {}
  canActivate(context: ExecutionContext): boolean {
    const isPublic = this.reflector.getAllAndOverride<boolean>(
      IS_GRAPH_PUBLIC_KEY,
      [context.getHandler(), context.getClass()],
    );
    if (isPublic) {
      return true;
    }

    const ctx = GqlExecutionContext.create(context);
    const request = ctx.getArgByIndex(2).req;
    const Authorization = request.get('Authorization');

    if (Authorization) {
      const token = Authorization.replace('Bearer ', '');
      const user = this.jwtService.verify(token);
      if (!!user) {
        Reflect.defineMetadata(USER, user, context.getHandler());
      }
      return !!user;
    }
  }
}
