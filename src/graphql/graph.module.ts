import { Module } from '@nestjs/common';
import { GraphUserResolver } from './resolvers/graph.user.resolver';
import { UserModule } from '../user/user.module';
import { ActivityModule } from '../activity/activity.module';
import { DeviceModule } from '../device/device.module';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from '../auth/constants';
import { GraphDeviceResolver } from './resolvers/graph.device.resolver';
import { GraphActivityResolver } from './resolvers/graph.activity.resolver';

@Module({
  imports: [
    UserModule,
    ActivityModule,
    DeviceModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '30 days' },
    }),
  ],
  providers: [GraphUserResolver, GraphDeviceResolver, GraphActivityResolver],
  exports: [],
})
export class GraphModule {}
