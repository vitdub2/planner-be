import { Injectable } from '@nestjs/common';
import { Activity } from '../schema/activity.schema';
import { ActivityRepository } from './activity.repository';
import { CrudService } from '../utils/crud.service';

@Injectable()
export class ActivityService extends CrudService<Activity> {
  constructor(private readonly activityRepository: ActivityRepository) {
    super(activityRepository);
  }
}
