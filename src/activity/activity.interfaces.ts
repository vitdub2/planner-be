import { IsArray, IsNumber, IsString, Length, Max, Min } from 'class-validator';
import { Optional } from '@nestjs/common';
import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class ActivityDto {
  @IsString()
  @Field()
  name: string;

  @Optional()
  @IsString()
  @Field({ nullable: true })
  desc: string;

  @IsArray()
  @Field((type) => [TimeDto])
  times: TimeDto[];
}

@InputType()
export class TimeDto {
  @IsNumber()
  @Min(1)
  @Max(7)
  @Field()
  day: number;

  @IsString()
  @Length(5, 5)
  @Field()
  startTime: string;

  @IsString()
  @Length(5, 5)
  @Field()
  endTime: string;
}
