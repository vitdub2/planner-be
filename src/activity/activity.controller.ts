import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { MODULE_NAMES } from '../utils/constants';
import { ActivityService } from './activity.service';
import { UserID } from '../utils/user-id.decorator';
import { Activity } from '../schema/activity.schema';
import { ActivityGuard } from './activity.guard';
import { ValidationPipe } from '../utils/validation.pipe';
import { ActivityDto } from './activity.interfaces';

@Controller(MODULE_NAMES.ACTIVITY)
export class ActivityController {
  constructor(private readonly activityService: ActivityService) {}

  @Get()
  @UseGuards(ActivityGuard)
  getActivities(@UserID() userId: string): Promise<Activity[]> {
    return this.activityService.getAllEntitiesByUserId(userId);
  }

  @Get(':id')
  @UseGuards(ActivityGuard)
  getActivity(@Param('id') activityId: string): Promise<Activity> {
    return this.activityService.getEntity(activityId);
  }

  @Post()
  createActivity(
    @Body(new ValidationPipe()) createDto: ActivityDto,
    @UserID() userId: string,
  ): Promise<Activity> {
    return this.activityService.createEntity(createDto, userId);
  }

  @Put(':id')
  @UseGuards(ActivityGuard)
  changeActivity(
    @Body(new ValidationPipe()) createDto: ActivityDto,
    @UserID() userId: string,
    @Param('id') activityId: string,
  ): Promise<Activity> {
    return this.activityService.changeEntity(createDto, userId, activityId);
  }

  @Delete(':id')
  @UseGuards(ActivityGuard)
  deleteActivity(@Param('id') activityId: string): Promise<Activity> {
    return this.activityService.deleteEntity(activityId);
  }
}
