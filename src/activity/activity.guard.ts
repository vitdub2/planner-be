import { Injectable } from '@nestjs/common';
import { EntityGuard } from '../utils/entity.guard';
import { Activity } from '../schema/activity.schema';
import { ActivityRepository } from './activity.repository';

@Injectable()
export class ActivityGuard extends EntityGuard<Activity> {
  constructor(private readonly activityRepository: ActivityRepository) {
    super(activityRepository);
  }
}
