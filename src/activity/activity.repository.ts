import { Injectable } from '@nestjs/common';
import { BaseRepository } from '../utils/base.repository';
import { Activity, ActivityDocument } from '../schema/activity.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

@Injectable()
export class ActivityRepository extends BaseRepository<Activity> {
  constructor(
    @InjectModel(Activity.name) private activityModel: Model<ActivityDocument>,
  ) {
    super(activityModel);
  }
}
