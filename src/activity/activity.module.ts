import { Module } from '@nestjs/common';
import { SchemasModule } from '../schema/schemas.module';
import { ActivityService } from './activity.service';
import { ActivityController } from './activity.controller';
import { ActivityRepository } from './activity.repository';

@Module({
  imports: [SchemasModule],
  providers: [ActivityRepository, ActivityService],
  controllers: [ActivityController],
  exports: [ActivityService],
})
export class ActivityModule {}
