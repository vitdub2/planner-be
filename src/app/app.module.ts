import { Global, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { MongooseModule } from '@nestjs/mongoose';
import * as dotenv from 'dotenv-json';
import { SchemasModule } from '../schema/schemas.module';
import { APP_GUARD } from '@nestjs/core';
import { JwtAuthGuard } from '../auth/jwt.auth.guard';
import { AuthModule } from '../auth/auth.module';
import { UserModule } from '../user/user.module';
import { ActivityModule } from '../activity/activity.module';
import { DeviceModule } from '../device/device.module';
import { ScheduleModule } from '@nestjs/schedule';
import { GraphQLModule } from '@nestjs/graphql';
import { GraphModule } from '../graphql/graph.module';
dotenv({ path: './.env.json' });

@Global()
@Module({
  imports: [
    MongooseModule.forRoot(process.env.mongo_url),
    SchemasModule,
    AuthModule,
    UserModule,
    ActivityModule,
    DeviceModule,
    ScheduleModule.forRoot(),
    GraphQLModule.forRoot({
      autoSchemaFile: true,
      sortSchema: true,
    }),
    GraphModule,
  ],
  controllers: [AppController],
  providers: [
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
  ],
})
export class AppModule {}
