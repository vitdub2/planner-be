import {
  BadRequestException,
  ExecutionContext,
  Injectable,
} from '@nestjs/common';
import { BaseRepository } from './base.repository';
import { IWithUserID } from './interfaces';

@Injectable()
export class EntityGuard<T extends IWithUserID> {
  constructor(private readonly entityRepository: BaseRepository<T>) {}

  async canActivate(context: ExecutionContext): Promise<any> {
    const request = context.switchToHttp().getRequest();
    const userId = request.user._id;
    const entityId = request.params.id;
    try {
      const entity = await this.entityRepository.findById(entityId);
      const id = entity.userId.toString();
      if (id === userId) {
        return true;
      }
    } catch (e) {
      throw new BadRequestException('Неверный ID');
    }
  }
}
