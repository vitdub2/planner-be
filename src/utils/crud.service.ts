import { Injectable } from '@nestjs/common';
import { BaseRepository } from './base.repository';
import { IWithUserID } from './interfaces';

@Injectable()
export abstract class CrudService<T extends IWithUserID> {
  protected constructor(private readonly entityRepository: BaseRepository<T>) {}

  getAllEntitiesByUserId(userId: string): Promise<T[]> {
    return this.entityRepository.findBy({ userId });
  }

  getEntity(entityId: string): Promise<T> {
    return this.entityRepository.findById(entityId);
  }

  createEntity(createDto: Partial<T>, userId: string): Promise<T> {
    return this.entityRepository.create({ ...createDto, userId });
  }

  changeEntity(
    changeDto: Partial<T>,
    userId: string,
    entityId: string,
  ): Promise<T> {
    return this.entityRepository.update({ userId, ...changeDto }, entityId);
  }

  deleteEntity(entityId: string): Promise<T> {
    return this.entityRepository.delete(entityId);
  }
}
