export const MODULE_NAMES = {
  USERS: 'users',
  AUTH: 'auth',
  CATEGORY: 'category',
  ACTIVITY: 'activity',
  DEVICE: 'device',
};
