import mongoose, { FilterQuery, Model } from 'mongoose';
import * as map from 'lodash/map';

export abstract class BaseRepository<T> {
  protected constructor(
    private readonly entityModel: Model<T & mongoose.Document>,
    private readonly mapFunc: (data: any) => any = (a) => a,
  ) {}

  async findAll(useMap = true): Promise<T[]> {
    const result = await this.entityModel.find().exec();
    return useMap ? map(result, this.mapFunc) : result;
  }

  async findById(id: string, useMap = true): Promise<T> {
    const result = await this.entityModel.findById(id).exec();
    return useMap ? this.mapFunc(result) : result;
  }

  async create(data: Partial<T>, useMap = true): Promise<T> {
    const entity = new this.entityModel(data);
    const result = await entity.save();
    return useMap ? this.mapFunc(result) : result;
  }

  async update(data: Partial<T>, id: string, useMap = true): Promise<any> {
    const result = await this.entityModel
      .findByIdAndUpdate(
        id,
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        //@ts-ignore
        { ...data },
        {
          useFindAndModify: false,
          new: true,
        },
      )
      .exec();
    return useMap ? this.mapFunc(result) : result;
  }

  async delete(id: string, useMap = true): Promise<any> {
    const result = await this.entityModel
      .findByIdAndDelete(id, {
        useFindAndModify: false,
      })
      .exec();
    return useMap ? this.mapFunc(result) : result;
  }

  async findOneBy(
    query: FilterQuery<T & mongoose.Document>,
    useMap = true,
  ): Promise<T> {
    const result = await this.entityModel.findOne(query).exec();
    return useMap ? this.mapFunc(result) : result;
  }

  async findBy(query: any, useMap = true): Promise<T[]> {
    const result = await this.entityModel.find(query).exec();
    return useMap ? map(result, this.mapFunc) : result;
  }
}
