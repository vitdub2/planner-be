import { BaseRepository } from '../utils/base.repository';
import { User, UserDocument } from '../schema/user.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { passwordCutter } from './password.cutter';

export class UserRepository extends BaseRepository<User> {
  constructor(@InjectModel(User.name) private userModel: Model<UserDocument>) {
    super(userModel, passwordCutter);
  }
}
