import { IsString } from 'class-validator';
import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class UserDto {
  @IsString()
  @Field()
  username: string;

  @IsString()
  @Field()
  email: string;
}

@InputType()
export class UserCreateDto extends UserDto {
  @IsString()
  @Field()
  password: string;
}
