import { Injectable, NotAcceptableException } from '@nestjs/common';
import { UserCreateDto, UserDto } from './user.interfaces';
import { User } from '../schema/user.schema';
import * as bcrypt from 'bcryptjs';
import { UserRepository } from './user.repository';
import * as isEmpty from 'lodash/isEmpty';

@Injectable()
export class UserService {
  constructor(private readonly userRepository: UserRepository) {}

  async findUserByLogin(login: string): Promise<User> {
    if (login.includes('@'))
      return this.userRepository.findOneBy({ email: login }, false);
    return this.userRepository.findOneBy({ username: login }, false);
  }

  async findUserById(id: string): Promise<User> {
    return this.userRepository.findById(id);
  }

  async createUser(userDto: UserCreateDto): Promise<any> {
    const salt = await bcrypt.genSalt();
    const password = await bcrypt.hash(userDto.password, salt);
    const registeredUsers = await this.userRepository.findBy({
      $or: [{ username: userDto.username }, { email: userDto.email }],
    });
    if (isEmpty(registeredUsers)) {
      return this.userRepository.create({
        ...userDto,
        password,
      });
    } else {
      throw new NotAcceptableException(
        'Пользователь с таким логином/email уже зарегистрирован',
      );
    }
  }

  async updateUser(id: string, userDto: UserDto): Promise<User> {
    return this.userRepository.update(userDto, id);
  }

  async deleteUser(id: string): Promise<User> {
    return this.userRepository.delete(id);
  }
}
