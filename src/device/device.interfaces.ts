import { IsBoolean, IsString } from 'class-validator';
import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class DeviceDto {
  @IsString()
  @Field()
  token: string;

  @IsString()
  @Field()
  name: string;

  @IsBoolean()
  @Field()
  shouldBeNotified: boolean;
}
