import { EntityGuard } from '../utils/entity.guard';
import { Device } from '../schema/device.schema';
import { DeviceRepository } from './device.repository';
import { Injectable } from '@nestjs/common';

@Injectable()
export class DeviceGuard extends EntityGuard<Device> {
  constructor(private readonly deviceRepository: DeviceRepository) {
    super(deviceRepository);
  }
}
