import { Module } from '@nestjs/common';
import { SchemasModule } from '../schema/schemas.module';
import { DeviceRepository } from './device.repository';
import { DeviceService } from './device.service';
import { DeviceController } from './device.controller';

@Module({
  imports: [SchemasModule],
  providers: [DeviceRepository, DeviceService],
  controllers: [DeviceController],
  exports: [DeviceService],
})
export class DeviceModule {}
