import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  UseGuards,
} from '@nestjs/common';
import { MODULE_NAMES } from '../utils/constants';
import { UserID } from '../utils/user-id.decorator';
import { ValidationPipe } from '../utils/validation.pipe';
import { DeviceService } from './device.service';
import { DeviceGuard } from './device.guard';
import { Device } from '../schema/device.schema';
import { DeviceDto } from './device.interfaces';

@Controller(MODULE_NAMES.DEVICE)
export class DeviceController {
  constructor(private readonly deviceService: DeviceService) {}

  @Get()
  @UseGuards(DeviceGuard)
  getActivities(@UserID() userId: string): Promise<Device[]> {
    return this.deviceService.getAllEntitiesByUserId(userId);
  }

  @Get(':id')
  @UseGuards(DeviceGuard)
  getActivity(@Param('id') activityId: string): Promise<Device> {
    return this.deviceService.getEntity(activityId);
  }

  @Post()
  createActivity(
    @Body(new ValidationPipe()) createDto: DeviceDto,
    @UserID() userId: string,
  ): Promise<Device> {
    return this.deviceService.createEntity(createDto, userId);
  }

  @Put(':id')
  @UseGuards(DeviceGuard)
  changeActivity(
    @Body(new ValidationPipe()) createDto: DeviceDto,
    @UserID() userId: string,
    @Param('id') activityId: string,
  ): Promise<Device> {
    return this.deviceService.changeEntity(createDto, userId, activityId);
  }

  @Delete(':id')
  @UseGuards(DeviceGuard)
  deleteActivity(@Param('id') activityId: string): Promise<Device> {
    return this.deviceService.deleteEntity(activityId);
  }
}
