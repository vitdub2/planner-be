import { BaseRepository } from '../utils/base.repository';
import { Device, DeviceDocument } from '../schema/device.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';

@Injectable()
export class DeviceRepository extends BaseRepository<Device> {
  constructor(
    @InjectModel(Device.name)
    private readonly deviceModel: Model<DeviceDocument>,
  ) {
    super(deviceModel);
  }
}
