import { CrudService } from '../utils/crud.service';
import { Device } from '../schema/device.schema';
import { DeviceRepository } from './device.repository';
import { Injectable } from '@nestjs/common';

@Injectable()
export class DeviceService extends CrudService<Device> {
  constructor(private readonly deviceRepository: DeviceRepository) {
    super(deviceRepository);
  }
}
