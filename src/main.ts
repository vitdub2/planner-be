import { NestFactory } from '@nestjs/core';
import * as admin from 'firebase-admin';
import * as dotenv from 'dotenv-json';
import { AppModule } from './app/app.module';

dotenv({ path: './.env.json' });

async function createNestServer() {
  const app = await NestFactory.create(AppModule, {
    cors: true,
  });

  const cert = {
    clientEmail: process.env.client_email,
    projectId: process.env.project_id,
    privateKey: process.env.private_key,
  };
  admin.initializeApp({
    credential: admin.credential.cert(cert as any),
  });

  app.enableCors();

  await app.listen(process.env.port || 4000);
}

createNestServer();
