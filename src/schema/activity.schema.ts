import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Time {
  @Prop({ required: true })
  @Field()
  day: number;

  @Prop({ required: true })
  @Field()
  startTime: string;

  @Prop({ required: true })
  @Field()
  endTime: string;
}

@Schema({ timestamps: true })
export class Activity {
  @Prop({ required: true, unique: true })
  name: string;

  @Prop({ required: false })
  desc: string;

  @Prop({ required: true, type: mongoose.SchemaTypes.ObjectId, ref: 'User' })
  userId: string;

  @Prop([Time])
  times: Time[];
}

export type ActivityDocument = Activity & mongoose.Document;

export const ActivitySchema = SchemaFactory.createForClass(Activity);
