import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { User, UserSchema } from './user.schema';
import { Activity, ActivitySchema } from './activity.schema';
import { Device, DeviceSchema } from './device.schema';

const schemas = [
  MongooseModule.forFeature([
    { name: User.name, schema: UserSchema },
    { name: Activity.name, schema: ActivitySchema },
    { name: Device.name, schema: DeviceSchema },
  ]),
];

@Module({
  imports: schemas,
  exports: schemas,
})
export class SchemasModule {}
