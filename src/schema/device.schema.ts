import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

@Schema({ timestamps: true })
export class Device {
  @Prop({ required: true, unique: true })
  token: string;

  @Prop({ required: true })
  name: string;

  @Prop({ required: true, type: mongoose.SchemaTypes.ObjectId, ref: 'User' })
  userId: string;

  @Prop({ required: true, default: true })
  shouldBeNotified: boolean;
}

export type DeviceDocument = Device & mongoose.Document;

export const DeviceSchema = SchemaFactory.createForClass(Device);
