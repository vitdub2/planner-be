import { IsString } from 'class-validator';

export interface IToken {
  token: string;
}

export interface IPassword {
  password: string;
}

export interface ILogin {
  login: string;
}

export class IDeviceInfo {
  @IsString()
  deviceId: string;
}
