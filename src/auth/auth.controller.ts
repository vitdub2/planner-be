import { Controller, Get, Post, UseGuards } from '@nestjs/common';
import { LocalAuthGuard } from './local.auth.guard';
import { AuthService } from './auth.service';
import { User } from '../utils/user.decorator';
import { Public } from '../utils/public.decorator';
import { MODULE_NAMES } from '../utils/constants';

@Controller(MODULE_NAMES.AUTH)
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('/login')
  @Public()
  @UseGuards(LocalAuthGuard)
  async login(@User() user) {
    return this.authService.login(user);
  }

  @Get('/profile')
  async getAuthorizedUser(@User() user) {
    return user;
  }
}
