import * as dotenv from 'dotenv-json';
dotenv({ path: './.env.json' });
export const jwtConstants = {
  secret: process.env.jwt_secret,
};
