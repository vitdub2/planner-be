import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserService } from '../user/user.service';
import { UserDto } from '../user/user.interfaces';
import { JwtService } from '@nestjs/jwt';
import { IToken } from './auth.interfaces';
import { passwordCutter } from '../user/password.cutter';
import * as bcrypt from 'bcryptjs';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(login: string, password: string): Promise<UserDto | null> {
    const user = await this.userService.findUserByLogin(login);
    if (user) {
      const isMatch = await bcrypt.compare(password, user.password);
      if (isMatch) {
        return user;
      } else {
        throw new UnauthorizedException('Неверный логин/пароль');
      }
    }
    throw new UnauthorizedException('Неверный логин/пароль');
  }

  async login(user: any): Promise<IToken> {
    if (user['_doc']) {
      user = passwordCutter(user);
    }
    return {
      token: this.jwtService.sign(user),
    };
  }
}
